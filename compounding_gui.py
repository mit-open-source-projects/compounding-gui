from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QPushButton, QWidget, QVBoxLayout


def view_compounding_results(days, expected_daily_compound, current_allocation, compounding_frequency):
    initial_capital = current_allocation
    results_string = "Initial Capital: $" + "{:,}".format(round(initial_capital, 2)) + "\n"

    i = 1
    while i < days + 1:
        current_allocation = current_allocation + (current_allocation * expected_daily_compound)
        results_string = results_string + "Day " + str(i) + ": $" + "{:,}".format(round(current_allocation, 2)) + "\n"
        i = i + compounding_frequency

    # print("\nEnding allocation (after " + str(days) + " days):", "{:,}".format(round(current_allocation, 2)))
    results_string = results_string + "\nInitial Capital: $" + "{:,}".format(round(initial_capital, 2)) + "\n"
    results_string = results_string + "Ending allocation (after " + str(days) + " days): $" + "{:,}".format(
        round(current_allocation, 2)) + "\n"

    return results_string


class App(QMainWindow):
    def __init__(self):
        super().__init__()
        self.title = 'Compounding Calculator'
        self.setWindowTitle(self.title)

        self.table_widget = MyTableWidget(self)
        self.setCentralWidget(self.table_widget)

        self.setMinimumSize(700, 700)

        self.show()


class MyTableWidget(QWidget):

    def __init__(self, parent):
        super(QWidget, self).__init__(parent)
        # self.layout = QVBoxLayout(self)

        self.results_box = QMessageBox()

        self.layout = QVBoxLayout(self)
        self.layout.setAlignment(Qt.AlignTop)

        self.titleLabel = QLabel("Calculate your returns on based on how fast they are compounding")
        self.titleLabel.setAlignment(Qt.AlignLeft)

        # ----------

        self.group1 = QGroupBox(self)
        self.p1Layout = QHBoxLayout(self)

        self.a1Label = QLabel("Initial Capital:")
        self.a1Input = QLineEdit("100000")
        self.a1Input.setMaximumWidth(380)

        self.p1Layout.addWidget(self.a1Label)
        self.p1Layout.addWidget(self.a1Input)

        self.group1.setLayout(self.p1Layout)

        # ---------------

        self.group2 = QGroupBox(self)
        self.p2Layout = QHBoxLayout(self)

        self.a2Label = QLabel("compounding Percent:")
        self.a2Input = QLineEdit("1")
        self.a2Input.setMaximumWidth(380)

        self.p2Layout.addWidget(self.a2Label)
        self.p2Layout.addWidget(self.a2Input)

        self.group2.setLayout(self.p2Layout)

        # ---------------

        self.group3 = QGroupBox(self)
        self.p3Layout = QHBoxLayout(self)

        self.a3Label = QLabel("Compounding Frequency (Trading Days):")
        self.a3Input = QLineEdit("1")
        self.a3Input.setMaximumWidth(380)

        self.p3Layout.addWidget(self.a3Label)
        self.p3Layout.addWidget(self.a3Input)

        self.group3.setLayout(self.p3Layout)

        # -------------

        self.group4 = QGroupBox(self)
        self.p4Layout = QHBoxLayout(self)

        self.a4Label = QLabel("Number of days to trade:")
        self.a4Input = QLineEdit("100")
        self.a4Input.setMaximumWidth(380)

        self.p4Layout.addWidget(self.a4Label)
        self.p4Layout.addWidget(self.a4Input)

        self.group4.setLayout(self.p4Layout)

        # -------------

        self.calcButton = QPushButton("Calculate returns")

        self.calcButton.clicked.connect(self.calculate_returns)

        self.results_label = QLabel("Results:")

        self.results_box = QTextEdit()
        self.results_box.setReadOnly(True)

        self.layout.addWidget(self.titleLabel)
        self.layout.addWidget(self.group1)
        # self.layout.addWidget(self.p2Label)
        self.layout.addWidget(self.group2)
        self.layout.addWidget(self.group3)
        self.layout.addWidget(self.group4)
        self.layout.addWidget(self.calcButton)
        self.layout.addWidget(self.results_label)
        self.layout.addWidget(self.results_box)

        # The way to constantly update the output
        self.auto_update_all()
        self.update_timer = QTimer()
        self.update_timer.timeout.connect(self.auto_update_all)
        self.update_timer.start(1000)

        # ---------------------------------------------------------------------------------------------------------------

    def auto_update_all(self):
        self.update()

    def calculate_returns(self):
        initial_capital = float(self.a1Input.text())
        compounding_percent = float(self.a2Input.text()) / 100
        compounding_frequency = int(self.a3Input.text())
        days_to_trade = int(self.a4Input.text())

        self.results_box.setText(view_compounding_results(days_to_trade, compounding_percent, initial_capital,
                                                          compounding_frequency))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())

