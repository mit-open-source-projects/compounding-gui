# Compounding GUI

A simple user interface for demonstrating how returns are affected based on the rate at which they compound. 

## Usage 

Just pip install the required packages listed at the top of the compounding_gui.py file and then
run the compounding_gui.py in python3. 